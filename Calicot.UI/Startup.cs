using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Calicot.Domain.Services;
using Calicot.Application;
using Calicot.AzureDB;

namespace Calicot.UI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            // Azure Storage credentials
            services.AddMvc();
            services.AddOptions();
            //services.Configure<AzureStorage.AzureStoreConfig>(Configuration.GetSection("StorageConfig"));

            
            var stacc = Configuration["StorageConfig:AccountName"];
            var stkey = Configuration["StorageConfig:AccountKey"];
            var stcont = Configuration["StorageConfig:ProductImageContainer"];

            var azStorage = new AzureStorage.AzureStoreConfig()
            {
                AccountName = stacc,
                AccountKey = stkey,
                ProductImageContainer = stcont
            };

            services.AddSingleton<AzureStorage.AzureStoreConfig>(azStorage);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddMvc().AddRazorPagesOptions(options =>
            {
                options.Conventions.AddPageRoute("/Produits", "");
            });

            // Register inMemory database
            //services.AddDbContext<CalicotInMemoryContext>(opt => opt.UseInMemoryDatabase());
            services.AddDbContext<CalicotAzureContext>(opt => 
            opt.UseSqlServer(Configuration.GetConnectionString("DefautConnection")));

            services.AddScoped<IProduitsService, ProduitsService>();

            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            // Remplir base donnes test
            //var context = app.ApplicationServices.GetService<CalicotInMemoryContext>();
            //RemplirBaseTest.RemplissageBaseTest(context);
            
            /// initialize database
            using (var scope = app.ApplicationServices.CreateScope())
            {
                var context = scope.ServiceProvider.GetService<CalicotAzureContext>();
                PremiereCharge.ChargerBaseDeDonnes(context);
                //.RemplissageBaseTest(context);
            }

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "/Produits/Index");
            });

            app.UseMvc();
        }
    }
}
