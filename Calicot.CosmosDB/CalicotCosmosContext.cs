﻿using Calicot.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Calicot.CosmosDB
{
    public class CalicotCosmosContext : DbContext
    {
        public CalicotCosmosContext(DbContextOptions<CalicotCosmosContext> options): base(options)
        {

        }
        public DbSet<Produit> Produit { get; set; }
    }
}
