﻿using Calicot.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Calicot.UI.Controllers
{
    public class ListsController : Controller
    {
        private readonly IProduitsService produitsService;

        public ListsController(IProduitsService produitsService)
        {
            this.produitsService = produitsService ?? throw new ArgumentNullException(nameof(produitsService));
        }

        public async Task<ActionResult> Index()
        {
            var listeProduits = await produitsService.ObtenirTousProduits();
            return View(listeProduits);
        }
    }
}
