﻿// <auto-generated />
using System;
using Calicot.AzureDB;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Calicot.AzureDB.Migrations
{
    [DbContext(typeof(CalicotAzureContext))]
    partial class CalicotAzureContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.14-servicing-32113")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Calicot.Domain.Models.Produit", b =>
                {
                    b.Property<Guid>("ProduitID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Couleur")
                        .IsRequired()
                        .HasColumnType("varchar(20)");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Marque")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<string>("Modele")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<string>("Nom")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.Property<string>("PhotoURL")
                        .HasColumnType("nvarchar(max)");

                    b.Property<decimal>("Prix")
                        .HasColumnType("decimal(5,2)");

                    b.Property<int>("Quantite")
                        .HasColumnType("int");

                    b.Property<string>("SKU")
                        .IsRequired()
                        .HasColumnType("varchar(10)");

                    b.Property<string>("Taille")
                        .IsRequired()
                        .HasColumnType("varchar(10)");

                    b.HasKey("ProduitID");

                    b.ToTable("Produits","dbo");
                });
#pragma warning restore 612, 618
        }
    }
}
