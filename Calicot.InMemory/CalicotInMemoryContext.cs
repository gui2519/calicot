﻿using Calicot.Domain.Models;
using Microsoft.EntityFrameworkCore;


namespace Calicot.InMemory
{
    public class CalicotInMemoryContext:DbContext
    {
        public CalicotInMemoryContext(DbContextOptions<CalicotInMemoryContext> options):base(options)
        {
        }
        public DbSet<Produit> Produit { get; set; }
    }
}
