﻿using Calicot.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Calicot.AzureDB
{
    public class CalicotAzureContext : DbContext
    {
        public CalicotAzureContext(DbContextOptions<CalicotAzureContext> options): base(options)
        {
        }

        public DbSet<Produit> Produit { get; set; }
    }
}
