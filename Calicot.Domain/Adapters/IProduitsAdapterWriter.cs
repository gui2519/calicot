﻿using Calicot.Domain.Models;
using System.Threading.Tasks;

namespace Calicot.Domain.Adapters
{
    public interface IProduitsAdapterWriter
    {
        /// <summary>
        /// Ajoute un nouveaou produit à la base de donnès
        /// </summary>
        /// <param name="produit">Produit a étre ajouté</param>
        /// <returns></returns>
        Task<int> AjouterProduit(Produit produit);
     
        /// <summary>
        /// Mettre a jour un nouveaou produit à la base de donnès
        /// </summary>
        /// <param name="produit">Produit a étre ajouté</param>
        /// <returns></returns>
        Task<int> UpdateProduit(Produit produit);
    }

}
