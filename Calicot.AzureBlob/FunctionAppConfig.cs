﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calicot.AzureStorage
{
    public class FunctionAppConfig
    {
        public string thumbWidth { get; set; }
        public string thumbContainerName { get; set; }
        public string AzureWebJobsStorage { get; set; }
        public string AzureWebJobsDashboard { get; set; }
        public string functionWebAppName { get; set; }
    }
}
