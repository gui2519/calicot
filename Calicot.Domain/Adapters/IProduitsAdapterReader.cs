﻿using Calicot.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calicot.Domain.Adapters
{
    public interface IProduitsAdapterReader
    {
        /// <summary>
        /// Obtien les produits a partir de la base de donnèes
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<Produit>> ObtenirTousProduits();

        /// <summary>
        /// Obtien un produit a partir de la base de donnèes
        /// </summary>
        /// <param name="guid"> Identification du produit </param>
        /// <returns></returns>
        Task<Produit> ObtenirProduitParID(Guid guid);
    }
}
