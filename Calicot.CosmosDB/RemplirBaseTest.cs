﻿using Calicot.Domain.Models;
using System;
using System.Threading.Tasks;

namespace Calicot.CosmosDB
{
    public class RemplirBaseTest
    {
        public static bool VerifierBasedeDonnes(CalicotCosmosContext ctx)
        {
            var dbexists = ctx.Database.EnsureCreated();
            return dbexists;
        }

        public static void RemplissageBaseTest(CalicotCosmosContext context)
        {
            Produit produit1 = new Produit()
            {
                Nom = "Molleton capuchon",
                ProduitID = new Guid(),
                SKU = "ARD-MLCAP-PK-P-C1",
                Marque = "Ard",
                Modele = "MLAP",
                Couleur = "Rose",
                Taille = "S",
                Prix = (decimal)15.99,
                Quantite = 159,
                PhotoURL = "https://www.ardene.com/dw/image/v2/BBVB_PRD/on/demandware.static/-/Sites-master-catalog/default/dwf1ebc5b2/images/large/1A-AP32643-01-99d8f005-41df-4734-93a3-64fcea61a06d.jpg?sw=1333&sh=2000&sm=fit",
                Description = "Morceau exclusif de notre collection Basic. Molleton ample. Capuchon à cordon. Manches longues avec épaules tombantes. Pochette kangourou. Doublure en polar. Le mannequin porte la taille P."
            };

            // Ajoute a la base
            context.Add(produit1);

            Produit produit2 = new Produit()
            {
                Nom = "Pantoufles glissières en tricot torsadé",
                ProduitID = new Guid(),
                SKU = "ARD-PANTT-GR-P-A2",
                Marque = "Ard",
                Modele = "PANTT",
                Couleur = "Gris",
                Taille = "M",
                Prix = (decimal)10.99,
                Quantite = 80,
                PhotoURL = "https://www.ardene.com/dw/image/v2/BBVB_PRD/on/demandware.static/-/Sites-master-catalog/default/dw6de72420/images/large/0B-FW30525-10-f6ef9375-29dd-4f05-b484-afbc34bc150b.jpg?sw=1333&sh=2000&sm=fit",
                Description = "Pantoufles glissières. Détail en tricot torsadé. Doublure en faux sherpa. Semelle intérieure en mousse mémoire. Détail de pompon. Semelle extérieure antidérapante.Pour les tailles 5 ou 6, choisis 5.5. Pour les tailles 7 ou 8, choisis 7.5. Pour les tailles 9 ou 10, choisis 9.5.",
            };

            // Ajoute a la base
            context.Add(produit2);

            Produit produit3 = new Produit()
            {
                Nom = "Botte sport Adventure 2.0 EK+ imperméable",
                ProduitID = new Guid(),
                SKU = "SIM-BOTT-NR-P-A3",
                Marque = "SIM",
                Modele = "BOTT",
                Couleur = "Noir",
                Taille = "M",
                Prix = (decimal)149.99,
                Quantite = 0,
                PhotoURL = "https://i.ebayimg.com/images/g/jBkAAOSw7aBVIi8s/s-l1600.jpg",
                Description = @"Réduisez votre empreinte environnementale, et multipliez vos empreintes de pas dans la neige!
                                -Construction imperméable alliant cuir LITE * et tissu ReBOLT MC Corduraᴹᴰ fait d'au moins 50 % de plastique recyclé et ultrarésistant à l'abrasion et aux déchirures
                                -faite à 50 % de plastique recyclé
                                -respirante et antimicrobienne
                                -Semelle extérieure en caoutchouc à relief antidérapant",
            };

            // Ajoute a la base
            context.Add(produit3);

            Produit produit4 = new Produit()
            {
                Nom = "Le T-shirt rouge",
                ProduitID = new Guid(),
                SKU = "SIM-SHRT-ROU-P-H9",
                Marque = "SIM",
                Modele = "SHIRT",
                Couleur = "Rouge",
                Taille = "M",
                Prix = (decimal)39.99,
                Quantite = 35,
                PhotoURL = "https://www.sportyfied.com/thumbs/regular/029032_35_basict_f_700x700.png",
                Description = @"Peu importe où, comment et pourquoi, assure-toi de faire vibrer ta fibre artistique",
            };

            // Ajoute a la base
            context.Add(produit4);


            // Sauvgader changements
            context.SaveChanges();
        }
    }
}
