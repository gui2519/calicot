﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Calicot.Domain.Services;
using Calicot.UI.ViewModel;
using System.Globalization;

namespace Calicot.UI.Controllers
{
    public class ProduitsController : Controller
    {
        private readonly IProduitsService produitsService;

        public ProduitsController(IProduitsService produitsService)
        {
            this.produitsService = produitsService ?? throw new ArgumentNullException(nameof(produitsService));
        }

        [Route("Produits/")]
        // GET: ProduitsController
        public async Task<ActionResult> Index()
        {
            var listeProduits = await produitsService.ObtenirTousProduits();
            return View(listeProduits);
        }

        [Route("Produits/Details/{id}")]
        // GET: ProduitsController/Details/5
        public async Task<ActionResult> Details(Guid id)
        {
            var produit = await produitsService.ObtenirProduitParID(id);
            return View(produit);
        }

        [Route("Produits/Create")]
        // GET: ProduitsController/Create
        public async Task<ActionResult> Create()
        {
            var produit = new Domain.Models.Produit();
            if (!ModelState.IsValid)
            {
                return View(produit);
            }

            return View(produit);
            //return RedirectToPage("./Index");
        }

        // POST: ProduitsController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ProduitsController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ProduitsController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ProduitsController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ProduitsController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [Route("Produits/Add")]
        [HttpGet]
        public ActionResult Add()
        {
            var produitView = new ProduitViewModel();
            return View(produitView);
        }


        [Route("Produits/Add")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(ProduitViewModel produitView)
        {
            // TODO upload file..

            string blobUrl = string.Empty;

            if (produitView.FormFile.Length > 0)
            {
                using (var stream = produitView.FormFile.OpenReadStream())
                {
                    blobUrl = await produitsService.UploadProduitPhoto(stream, produitView.FormFile.FileName);
                }
            }


            // map produit

            Domain.Models.Produit produit = new Domain.Models.Produit(){
                SKU = produitView.SKU,
                Description = produitView.Description,
                Couleur = produitView.Couleur,
                Marque = produitView.Marque,
                Nom = produitView.Nom,
                Modele = produitView.Modele,
                Prix = Convert.ToDecimal(produitView.Prix, CultureInfo.InvariantCulture),
                Quantite = produitView.Quantite,
                Taille = produitView.Taille.ToString(),
                PhotoURL = blobUrl
            };

            // Add to db

            var result = await produitsService.AjouterProduit(produit);

            
            //return Json(new { Status = "success", Message = "Produit ajouté" });
            

            return RedirectToAction(nameof(Index));
        }
    }
}
