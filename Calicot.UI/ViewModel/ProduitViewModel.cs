﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Calicot.UI.ViewModel
{
    public enum Tailles
    {
        XP = 0,
        P = 1,
        M = 2,
        G = 3,
        XG = 4,
        XXG = 5
    }
    public class ProduitViewModel
    {
        [DisplayName("SKUs")]
        public string SKU { get; set; }
        public string Nom { get; set; }
        public string Marque { get; set; }
        public string Modele { get; set; }
        public string Couleur { get; set; }

        [EnumDataType(typeof(Tailles))]
        public Tailles Taille { get; set; }
        public string Description { get; set; }
        public int Quantite { get; set; }
        public decimal Prix { get; set; }
        [DisplayName("Telecharcher Fichier")]
        public IFormFile FormFile { get; set; }
    }
}
