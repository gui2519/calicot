﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Calicot.Domain.Models;
using Calicot.Domain.Services;
using Calicot.AzureDB;
using Calicot.Database;
using Calicot.AzureStorage;
using System.IO;
using Microsoft.Extensions.Options;

namespace Calicot.Application
{
    public class ProduitsService : IProduitsService
    {
        private readonly CalicotAzureContext dbContext;
        private readonly AzureStoreConfig _storageConfig = null;

        public ProduitsService(CalicotAzureContext context, AzureStoreConfig azureStoreConfig)
        {
            this.dbContext = context ?? throw new ArgumentNullException(nameof(context));
            this._storageConfig = azureStoreConfig ?? throw new ArgumentNullException(nameof(azureStoreConfig));
        }

        public async Task<int> AjouterProduit(Produit produit)
        {
            var adapter = new ProduitsAdapter(dbContext);
            return await adapter.AjouterProduit(produit);

            //throw new NotImplementedException();
        }

        public async Task<int> MettreAjourProduit(Produit produit)
        {
            var adapter = new ProduitsAdapter(dbContext);
            return await adapter.UpdateProduit(produit);

            //throw new NotImplementedException();
        }

        public async Task<Produit> ObtenirProduitParID(Guid guid)
        {
            var adapter = new ProduitsAdapter(dbContext);
            return await adapter.ObtenirProduitParID(guid);

            //throw new NotImplementedException();
        }

        public async Task<IEnumerable<Produit>> ObtenirTousProduits()
        {
            var adapter = new ProduitsAdapter(dbContext);
            return await adapter.ObtenirTousProduits();

            //throw new NotImplementedException();
        }

        public async Task<string> UploadProduitPhoto(Stream filestream, string filename)
        {
            var storageManager = new BlobManager(_storageConfig);

            var jsonImage =  await storageManager.Upload(filestream, Guid.NewGuid().ToString() + filename);

            return jsonImage.Url;
        }
    }
}
