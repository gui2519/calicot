﻿using Calicot.Domain.Adapters;
using Calicot.Domain.Models;
using Calicot.AzureDB;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calicot.Database
{
    public class ProduitsAdapter : IProduitsAdapterReader, IProduitsAdapterWriter
    {
        private readonly CalicotAzureContext _context;

        public ProduitsAdapter(CalicotAzureContext context)
        {
            this._context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<int> AjouterProduit(Produit produit)
        {
            _context.Produit.Add(produit);
            return await _context.SaveChangesAsync();

            //throw new NotImplementedException();
        }

        public async Task<Produit> ObtenirProduitParID(Guid guid)
        {
            return await _context.Produit.FirstOrDefaultAsync(m => m.ProduitID == guid);
            //throw new NotImplementedException();
        }

        public async Task<IEnumerable<Produit>> ObtenirTousProduits()
        {
            return await _context.Produit.ToListAsync();
            //throw new NotImplementedException();
        }

        public async Task<int> UpdateProduit(Produit produit)
        {
            try
            {
                return await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProduitExists(produit.ProduitID))
                {
                    return 300;
                }
                else
                {
                    throw;
                }
            }

            
            //throw new NotImplementedException();
        }

        private bool ProduitExists(Guid id)
        {
            return _context.Produit.Any(e => e.ProduitID == id);
        }
    }
}
