﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Calicot.Domain.Models
{

    [Table("Produits", Schema = "dbo")]
    public class Produit
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ProduitID { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(20)")]
        [Display(Name = "SKU")]
        public string SKU { get; set; }

        [Required]
        [Column(TypeName = "varchar(100)")]
        [Display(Name = "Nom")]
        public string Nom { get; set; }

        [Required]
        [Column(TypeName = "varchar(50)")]
        [Display(Name = "Marque")]
        public string Marque { get; set; }

        [Required]
        [Column(TypeName = "varchar(50)")]
        [Display(Name = "Modele")]
        public string Modele { get; set; }

        [Required]
        [Column(TypeName = "varchar(20)")]
        [Display(Name = "Coleur")]
        public string Couleur { get; set; }

        [Required]
        [Column(TypeName = "varchar(10)")]
        [Display(Name = "Taille")]
        public string Taille { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(max)")]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required]
        [Column(TypeName = "int")]
        [Display(Name = "Quantite")]
        public int Quantite { get; set; }

        [Required]
        [Column(TypeName = "decimal(5,2)")]
        [Display(Name = "Prix")]
        public decimal Prix { get; set; }


        [Column(TypeName = "nvarchar(max)")]
        [Display(Name = "PhotoURL")]
        public string PhotoURL { get; set; }
    }
}
