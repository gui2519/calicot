﻿namespace Calicot.AzureStorage
{
    public class AzureStoreConfig
    {
        public string AccountName { get; set; }
        public string AccountKey { get; set; }
        public string ProductImageContainer { get; set; }
    }
}