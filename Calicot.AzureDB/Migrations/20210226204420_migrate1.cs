﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Calicot.AzureDB.Migrations
{
    public partial class migrate1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "dbo");

            migrationBuilder.CreateTable(
                name: "Produits",
                schema: "dbo",
                columns: table => new
                {
                    ProduitID = table.Column<Guid>(nullable: false),
                    SKU = table.Column<string>(type: "varchar(10)", nullable: false),
                    Nom = table.Column<string>(type: "varchar(100)", nullable: false),
                    Marque = table.Column<string>(type: "varchar(50)", nullable: false),
                    Modele = table.Column<string>(type: "varchar(50)", nullable: false),
                    Couleur = table.Column<string>(type: "varchar(20)", nullable: false),
                    Taille = table.Column<string>(type: "varchar(10)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Quantite = table.Column<int>(type: "int", nullable: false),
                    Prix = table.Column<decimal>(type: "decimal(5,2)", nullable: false),
                    PhotoURL = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Produits", x => x.ProduitID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Produits",
                schema: "dbo");
        }
    }
}
