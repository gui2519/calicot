﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calicot.AzureStorage
{
    public class  BlobManager
    {
        private AzureStoreConfig _storeConfig;
        public BlobManager(AzureStoreConfig storeConfig)
        {
            _storeConfig = storeConfig;
        }

        public async Task<JsonImage> Upload(Stream stream, string filename)
        {
            var url = await UploadFileStorage(stream, filename, _storeConfig);

            return new JsonImage()
            {
                Url = url,
            };
        }


        private static async Task<string> UploadFileStorage(Stream filestream, string fileName, AzureStoreConfig stconfig)
        {
            var storageCredentials = new StorageCredentials(stconfig.AccountName, stconfig.AccountKey);
            var storageAccount = new CloudStorageAccount(storageCredentials, true);

            var blobClient = storageAccount.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference(stconfig.ProductImageContainer);
            var blockBlob = container.GetBlockBlobReference(fileName);

            await blockBlob.UploadFromStreamAsync(filestream);

            return blockBlob.SnapshotQualifiedStorageUri.PrimaryUri.ToString();
        }

        public static bool IsImage(string nameFile)
        {
            string[] formats = new string[] { ".jpg", ".png", ".gif", ".jpeg" };
            return formats.Any(item => nameFile.EndsWith(item, StringComparison.OrdinalIgnoreCase));
        }
    }
}
