﻿using Calicot.Domain.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Calicot.Domain.Services
{
    public  interface IProduitsService
    {
        /// <summary>
        /// Retrouve tous produits de la magazine
        /// </summary>
        /// <returns>
        /// Liste (IEnumerable) avec tous produits de la magaine
        /// </returns>
        Task<IEnumerable<Produit>> ObtenirTousProduits();

        /// <summary>
        /// Retrouve les detais d'une produit on forunissant l'id
        /// </summary>
        /// <param name="guid">Identification du produit</param>
        /// <returns>Produit</returns>
        Task<Produit> ObtenirProduitParID(Guid guid);

        /// <summary>
        /// Ajoute un nouveaou produit à la base de donnès
        /// </summary>
        /// <param name="produit"></param>
        /// <returns></returns>
        Task<int> AjouterProduit(Produit produit);

        /// <summary>
        /// Mettre a jour un nouveaou produit à la base de donnès
        /// </summary>
        /// <param name="produit"></param>
        /// <returns></returns>
        Task<int> MettreAjourProduit(Produit produit);

        /// <summary>
        ///  Telechager l'image d'un produits
        /// </summary>
        /// <param name="filestream"></param>
        /// <returns></returns>
        Task<string> UploadProduitPhoto(Stream filestream, string filename);
    }
}
